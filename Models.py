import torch
import torch.nn as nn

class LSTM(nn.Module):
    def __init__(self, input_dim, hidden_dim, layer_dim, output_dim):
        super().__init__()
        self.num_layers = layer_dim
        self.hidden_size = hidden_dim
        self.lstm = nn.LSTM(input_dim, hidden_dim, layer_dim, batch_first=True)
        self.fc = nn.Linear(hidden_dim, output_dim)
        # self.sigm = nn.Sigmoid()

    def init_hidden(self, x):      
        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size) 
        c0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size)
        return h0, c0

    def forward(self, x, h0, c0):

        out, _ = self.lstm(x, (h0,c0))  
        out = out[:, -1, :]
        out = self.fc(out)
        # out = self.sigm(out)
        return out
    
class GRU(nn.Module):
    def __init__(self, input_dim, hidden_dim, layer_dim, output_dim):
        super().__init__()
        self.hidden_dim = hidden_dim
        self.layer_dim = layer_dim
        self.rnn = nn.GRU(input_dim, hidden_dim, layer_dim, batch_first=True)
        self.fc = nn.Linear(hidden_dim, output_dim)

    def init_hidden(self, x):
        h0 = torch.zeros(self.layer_dim, x.size(0), self.hidden_dim).requires_grad_()
        return h0

    def forward(self, x, h0):
        out, hn = self.rnn(x, h0)
        out = self.fc(out[:, -1, :]) 
        return out
    

class GRU_discovery(nn.Module):
    def __init__(self, input_dim, hidden_dim, layer_dim, output_dim, physics_constans):
        assert len(physics_constans) == 4
        super().__init__()
        self.hidden_dim = hidden_dim
        self.layer_dim = layer_dim
        self.rnn = nn.GRU(input_dim, hidden_dim, layer_dim, batch_first=True)
        self.fc = nn.Linear(hidden_dim, output_dim)

        self.l = nn.Parameter(data=torch.tensor(physics_constans[0], requires_grad=True))
        self.m_ball = nn.Parameter(data=torch.tensor(physics_constans[1], requires_grad=True))
        self.M_cart = nn.Parameter(data=torch.tensor(physics_constans[2], requires_grad=True))
        self.gamma1 = nn.Parameter(data=torch.tensor(physics_constans[3], requires_grad=True))

    def init_hidden(self, x):
        h0 = torch.zeros(self.layer_dim, x.size(0), self.hidden_dim).requires_grad_()
        return h0

    def forward(self, x, h0):
        out, hn = self.rnn(x, h0)
        out = self.fc(out[:, -1, :]) 
        return out
