load("data\water_tanks\24_11\24_11_pomiar_2.mat")

level1 = data.Level1 *100;
level2 = data.Level2 *100;
level3 = data.Level3 *100;

control = data.Control;

time = data.time;


subplot(4,1,1);
plot(time,level1)
hold on
title('Poziom w zbiorniku I');
xlabel('Czas (s)');
ylabel('Poziom wody (cm)');
grid on
xlim([0,3500])
ylim([0,18])
hold off;

subplot(4,1,2);
plot(time,level2)
hold on
title('Poziom w zbiorniku II');
xlabel('Czas (s)');
ylabel('Poziom wody (cm)');
grid on
xlim([0,3500])
ylim([0,18])
hold off;

subplot(4,1,3);
plot(time,level3)
hold on
title('Poziom w zbiorniku III');
xlabel('Czas (s)');
ylabel('Poziom wody (cm)');
grid on
xlim([0,3500])
ylim([0,18])
hold off;

subplot(4,1,4);
stairs(time, control, 'r')
hold on
title('Sterowanie pompy');
xlabel('Czas (s)');
ylabel('% pełnej mocy pompy');
grid on
xlim([0,3500])
ylim([0,1])
hold off;


set(gcf, 'Position',  [100, 100, 800, 700])
saveas(gcf,'./ryciny/water_tanks_plot.png')

