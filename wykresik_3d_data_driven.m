load("results\GRU_PINN_discovery_l_M_cart_m_cart.mat")
starting_value = 11;

x = physic_values(starting_value:2:end, 1);
y = physic_values(starting_value:2:end, 2);
z = physic_values(starting_value:2:end, 3);

result = physic_result(starting_value-1:2:end);
result = log(result+1);

plot3(x,y,z,'k-.')
hold on
scatter3(x,y,z,60,result,'filled', 'o','MarkerEdgeColor', 'k')

% xlim([0.9,2.2])
% ylim([3.5,7.5])
title('Zmiany wartości parametrów l, m i M w trakcie kolejnych epok uczenia');
xlabel('Parametr l');
ylabel('Parametr M');
plot3([1.5], [3], [5], 'rx', 'MarkerSize', 20)
grid on
colorbar
colormap jet
hold off;
