starting_value = 9;

load("results\GRU_PINN_discovery_l_m_ball_v1.mat")

x_1 = physic_values(starting_value:2:end, 1);
y_1 = physic_values(starting_value:2:end, 2);
result_1 = physic_result(starting_value-1:2:end);
result_1 = log(result_1+1);

disp(result_1)

load("results\GRU_PINN_discovery_l_m_ball_v2.mat")

x_2 = physic_values(starting_value:2:end, 1);
y_2 = physic_values(starting_value:2:end, 2);
result_2 = physic_result(starting_value-1:2:end);
result_2 = log(result_2+1);

load("results\GRU_PINN_discovery_l_m_ball_v3.mat")

x_3 = physic_values(starting_value:2:end, 1);
y_3 = physic_values(starting_value:2:end, 2);
result_3 = physic_result(starting_value-1:2:end);
result_3 = log(result_3+1);

load("results\GRU_PINN_discovery_m_ball_M_cart_v1.mat")

x_4 = physic_values(starting_value:2:end, 2);
y_4 = physic_values(starting_value:2:end, 3);
result_4 = physic_result(starting_value-1:2:end);
result_4 = log(result_4+1);

load("results\GRU_PINN_discovery_m_ball_M_cart_v2.mat")

x_5 = physic_values(starting_value:2:end, 2);
y_5 = physic_values(starting_value:2:end, 3);
result_5 = physic_result(starting_value-1:2:end);
result_5 = log(result_5+1);

load("results\GRU_PINN_discovery_m_ball_M_cart_v3.mat")

x_6 = physic_values(starting_value:2:end, 2);
y_6 = physic_values(starting_value:2:end, 3);
result_6 = physic_result(starting_value-1:2:end);
result_6 = log(result_6+1);

load("results\GRU_PINN_discovery_l_M_cart_v1.mat")

x_7 = physic_values(starting_value:2:end, 1);
y_7 = physic_values(starting_value:2:end, 3);
result_7 = physic_result(starting_value-1:2:end);
result_7 = log(result_7+1);

load("results\GRU_PINN_discovery_l_M_cart_v2.mat")

x_8 = physic_values(starting_value:2:end, 1);
y_8 = physic_values(starting_value:2:end, 3);
result_8 = physic_result(starting_value-1:2:end);
result_8 = log(result_8+1);

load("results\GRU_PINN_discovery_l_M_cart_v3.mat")

x_9 = physic_values(starting_value:2:end, 1);
y_9 = physic_values(starting_value:2:end, 3);
result_9 = physic_result(starting_value-1:2:end);
result_9 = log(result_9+1);

figure('Position', [100, 100, 700, 800]); % [left, bottom, width, height]

subplot(3,1,1);
plot(x_1,y_1,'k-.')
hold on
scatter(x_1,y_1,60,result_1,'filled', 'o','MarkerEdgeColor', 'k')
plot(x_2,y_2,'k-.')
scatter(x_2,y_2,60,result_2,'filled', 'd', 'MarkerEdgeColor', 'k') 
plot(x_3,y_3,'k-.')
scatter(x_3,y_3,60,result_3,'filled', '^', 'MarkerEdgeColor', 'k')
xlim([1.0,2])
ylim([2,4.8])
title('Zmiany wartości parametrów l i m w trakcie kolejnych epok uczenia');
xlabel('Parametr l');
ylabel('Parametr m');

plot([1.5], [3], 'rx', 'MarkerSize', 20)
grid on
colorbar
colormap cool
hold off;


subplot(3,1,2);
plot(x_4,y_4,'k-.')
hold on
scatter(x_4,y_4,60,result_4,'filled', 'o','MarkerEdgeColor', 'k')
plot(x_5,y_5,'k-.')
scatter(x_5,y_5,60,result_5,'filled', 'd', 'MarkerEdgeColor', 'k')
plot(x_6,y_6,'k-.')
scatter(x_6,y_6,60,result_6,'filled', '^', 'MarkerEdgeColor', 'k')
xlim([2.5,4.1])
ylim([3.8,6.8])
title('Zmiany wartości parametrów m i M w trakcie kolejnych epok uczenia');
xlabel('Parametr m');
ylabel('Parametr M');
plot([3], [5], 'rx', 'MarkerSize', 20)
grid on
colorbar
colormap cool
hold off;

subplot(3,1,3);
plot(x_7,y_7,'k-.')
hold on
scatter(x_7,y_7,60,result_7,'filled', 'o','MarkerEdgeColor', 'k')
plot(x_8,y_8,'k-.')
scatter(x_8,y_8,60,result_8,'filled', 'd', 'MarkerEdgeColor', 'k') 
plot(x_9,y_9,'k-.')
scatter(x_9,y_9,60,result_9,'filled', '^', 'MarkerEdgeColor', 'k')
xlim([0.9,2.2])
ylim([3.5,7.5])
title('Zmiany wartości parametrów l i M w trakcie kolejnych epok uczenia');
xlabel('Parametr l');
ylabel('Parametr M');
plot([1.5], [5], 'rx', 'MarkerSize', 20)
grid on
colorbar
colormap jet
hold off;

saveas(gcf,'./ryciny/data_discovery_1.png')
