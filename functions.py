import torch
import numpy as np
import torch.nn as nn
import torch.utils.data as utils
import torch.nn.functional as F
import matplotlib.pyplot as plt
from Models import LSTM, GRU
from tqdm import tqdm
from sklearn.metrics import mean_squared_error, mean_absolute_error

def generate_force_vect(num_of_experiments=4,num_of_timesteps_per_experiment = 1000, border_length = 20, max_force_abs_value = 50, seed = None):
     #border length describes number of timesteps at begin end end of experiment with F=0

    f_vect=[]
    current_exp_f_vect =[]

    if seed is not None:
        np.random.seed(seed)    

    for i in range(num_of_experiments):
        current_exp_f_vect = np.zeros(border_length).tolist()
        j=border_length+1
        while j < num_of_timesteps_per_experiment-border_length:
            force_length_exp_factor = np.random.randint(low=1, high=6)
            # force_length_exp_factor = 7
            tmp_vec = np.full(2**force_length_exp_factor, np.random.randint(low=-1*max_force_abs_value, high=max_force_abs_value*1))
            # tmp_vec *= 10
            if np.random.rand() < 0.1: #zapewnienie nadpopulacji zerowych wymuszeń
                tmp_vec = np.full(2**force_length_exp_factor, 0)
            current_exp_f_vect += tmp_vec.tolist()

            j += 2**force_length_exp_factor
        current_exp_f_vect = current_exp_f_vect[:num_of_timesteps_per_experiment-border_length]
        current_exp_f_vect += np.zeros(border_length).tolist()

        f_vect += current_exp_f_vect
        # print(f_vect)
    f_vect += np.zeros(border_length).tolist()
    return f_vect

def data_prep(path, seq_len=100, batch_size=256, data_shuffle=False, time_include=False):
    data = np.genfromtxt(path, delimiter=',', skip_header=5)

    force_vect = data[:, 0]
    angle_vect = data[:, 1]

    #normalization
    force_vect = force_vect / 50
    # angle_vect = angle_vect - angle_vect[0]

    #preparing timeseries data
    step_between_data_samples = 1

    force_samples = [[[force_vect[i]] for i in range(j,j+seq_len) ] for j in range(0, len(force_vect)-seq_len+1,step_between_data_samples)]
    force_samples = np.array(force_samples)
    
    angle_samples = angle_vect[seq_len-1::step_between_data_samples]
    angle_samples = np.array(angle_samples).reshape(-1,1)

    print(f'Min angle value {np.min(angle_samples)}, max angle value {np.max(angle_samples)}')

    # force_samples = torch.cat((torch.from_numpy(force_samples).float(),torch.from_numpy(time_samples).float()),dim=2)
    if time_include:
        time_samples = [[[(i%1000)/1000] for i in range(j,j+seq_len) ] for j in range(0, len(force_vect)-seq_len+1,step_between_data_samples)]
        time_samples = np.array(time_samples)
        force_samples = np.concatenate((force_samples,time_samples),axis=2)
    print(force_samples.shape)
    dataset = utils.TensorDataset(torch.from_numpy(force_samples).float(),torch.from_numpy(angle_samples).float())
    # test_dataset = utils.TensorDataset(torch.from_numpy(force_samples_test).float())

    data_loader = utils.DataLoader(dataset, batch_size=batch_size, shuffle=data_shuffle)

    return data_loader

def mae_eval(model, wal_loader, seq_len, device):
    model.eval()
    all_preds = []
    all_outs = []
    input_size = 1

    # val_mean_abs_errors = []
    for i, (x, y) in enumerate(wal_loader):
        x = x.view(-1, seq_len, input_size).requires_grad_().to(device)
        hidden = model.init_hidden(x).to(device)
        x, y = x.to(device), y.to(device)
        # print(x.shape, y.shape)
        preds = model(x, hidden) 
        preds = preds.reshape(x.size(0), -1)
        y = y.reshape(x.size(0), -1)
        all_preds.append(preds.detach().cpu().numpy())
        all_outs.append(y.detach().cpu().numpy())
    all_preds = np.concatenate(all_preds, axis=0)
    all_outs = np.concatenate(all_outs, axis=0)
    mae = mean_absolute_error(all_preds, all_outs)
    return mae
