u = zeros(1,10);
min_u = 55;
max_u = 90;



while(length(u)<1800)
    exp = randi([4,6],1,1);
    value = randi([min_u, max_u],1,1)/100;
    
    if rand() < 0.1
        value = 0;
        exp = 3;
    end
    
    u =[u, ones(1,2^exp)*value];
    
    
end

u2 = [1:1:length(u)];

wynik = [u2',u'];

plot(u)